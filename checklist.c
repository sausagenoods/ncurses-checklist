#include <ncurses.h>
#include "checklist.h"

typedef struct {
    int x;
    int y;
    int checked;
} checkbox_t;

int checklist(int y, int x, char *questions[], int lines, int boxes, int type, int lineno, int show_value)
{
    int col, row;
    getmaxyx(stdscr, col, row); // get term size
    
    if (lineno) {
        for (int i = 0; i < lines; i++) {
            mvprintw(y+i, 1, "%2d-", i+1);
            mvprintw(y+i, 4, questions[i]);  
        }
    }
                                                                       
    int len = 0, longest = 0;
    /* get the len of longest string */
    for (int j = 0; j < lines; j++) {
        for (int i = 0; questions[j][i] != '\0'; i++) {
            len++;
        }
        if (len > longest)
            longest = len;
        len = 0;
    }
    
    /* calculate the x pos and spaces between each checkbox */
    int spaces = (((row - x - longest) / boxes) < 5) ? spaces : 5;

    return checkbox(y, row - boxes * spaces, lines, boxes, spaces, type, show_value);
}

int checkbox(int y, int x, int lines, int boxes, int space, int type, int show_value)  // n is the number of checkboxes
{
    int i, k, c, y_pos, x_pos;
    checkbox_t blanks[lines][boxes];
    
    if (show_value) {
        for (int l = 0; l < boxes; l++) 
            mvprintw(y-1, x+l*space+1, "%d", l);
    }

    /* draw checkboxes */
    for (k = 0; k < lines; k++) {
        for (i = 0; i < boxes; i++) {
            mvprintw(y+k, x+i*space, "%c", (type)?'[':'(');
            mvprintw(y_pos = y+k, x_pos = x+i*space+1, "%c", ' '); // save xy in an array
            blanks[k][i].y = y_pos;
            blanks[k][i].x = x_pos;
            blanks[k][i].checked = 0;
            mvprintw(y+k, x+i*space+2, "%c", (type)?']':')');
        }
    }
      
    int line = 0, box = 0, score = 0;
    move(blanks[line][box].y, blanks[line][box].x);

    while ((c = getch()) != 'q') {
        switch (c) {
            case KEY_UP:
            case 'W':
            case 'w':
                if (line-1 >= 0)
                    line--;
                break;
            
            case KEY_DOWN:
            case 'S':
            case 's':
                if (line+1 < lines)
                    line++;
                break;
            
            case KEY_LEFT:
            case 'A':
            case 'a':
                if (box-1 >= 0)
                    box--;
                break;
            
            case KEY_RIGHT:
            case 'D':
            case 'd':
                if (box+1 < boxes)
                    box++;
                break;
           
            case 10:
            case ' ':
                for (int l = 0; l < boxes; l++) {
                    if (blanks[line][l].checked) {
                        mvprintw(blanks[line][l].y, blanks[line][l].x, "%c", ' ');
                        blanks[line][l].checked = 0;
                        score -= l;
                    }
                }
                
                if (!blanks[line][box].checked) {
                    mvprintw(blanks[line][box].y, blanks[line][box].x, "%c", 'x');
                    blanks[line][box].checked = 1;
                    score += box;
                } 
                break;
                
            default:
                break;
        } 

        move(blanks[line][box].y, blanks[line][box].x);
    }
    return score;
}
