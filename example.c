#include <ncurses.h>
#include "checklist.h"

#define num(x) (sizeof(x) / sizeof(x[0]))

void init_curses()
{
    initscr();
    noecho();
    raw();
    keypad(stdscr, TRUE);
}

int main()
{
    init_curses();

    char *test[2] = { "Type 1", "Type 2"};
    int line_no = num(test);

    mvprintw(0, 0, "This is a checklist");
    checklist(2, 0, test, 2, 1, PARENTHESIS, 1, 0);

    mvprintw(6, 0, "Checklists can have multiple choices, values ranging from 0 to n");
    int points = checklist(7, 0, test, 2, 4, BRACKET, 0, 1);
    mvprintw(9, 0, "Points: %d", points);
    mvprintw(11, 0, "This is a single checkbox");
    checkbox(12, 0, 1, 1, 0, 1, 0);                      

    endwin();
    return 0;
 }
